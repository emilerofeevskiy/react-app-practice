import React, {Component} from 'react';
import './App.css';
import Car from './Car/Car';
import ErrorBoundary from './ErrorBoundary/ErrorBoundary'
import Counter from './Counter/Counter'

export default class App extends Component {
	
	constructor(props) {
		super(props)

		this.state = {
			cars: [
				{name: 'Ford', year: '2018'},
				// {name: 'Audi', year: '2016'},
				// {name: 'Mazda', year: '2010'}
			],
			pageTitle: 'React components',
			showCars: false,
		}
	}

	toggleCarsHandler = () => {

		this.setState({
			showCars: !this.state.showCars
		})
	}

	onChangeName(name, id) 
	{
		const car = this.state.cars[id]
		car.name = name
		const cars = [...this.state.cars]
		cars[id] = car
		this.setState({
			cars
		})
	}

	deleteHandler(id){
		const cars = this.state.cars.concat()
		cars.splice(id, 1)

		this.setState({cars})
	}
	

	render(){
		let cars = null

		if (this.state.showCars)
		{
			cars = this.state.cars.map((car, id) => {
				return (
					<ErrorBoundary key={id}>
						<Car 
							name={car.name} 
							year={car.year}
							onDelete={this.deleteHandler.bind(this, id)} 
							onChangeName = {event => this.onChangeName(event.target.value, id)}
						/>
					</ErrorBoundary>
				)
			}) 
		}
		return (
		<div className="divStyle">
			{/* <h1>{this.state.pageTitle}</h1> */}
			<h1>{this.props.title}</h1>

			<Counter />

			<button className="main-btn" onClick={this.toggleCarsHandler}>Загрузить данные</button>
			<div className="cars">
				{ cars }
			</div>
		</div>
		);
	}
		
}

