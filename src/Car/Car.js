import React from 'react'
import Radium from 'radium'
import './Car.css'

class Car extends React.Component{

    // UNSAFE_componentWillReceiveProps(nextProps){
    //     console.log('willupdate', nextProps)
    // }

    shouldComponentUpdate(nextProps, nextState){
        console.log('shouldupdate', nextProps, nextState)
        return nextProps.name.trim() !== this.props.name.trim()
    }

//    UNSAFE_componentWillUpdate(nextProps, nextState){
//         console.log('will2update', nextProps, nextState)
//     }

    static getDerivedStateFromProps(nextProps, prevState){
        console.log('Car getDer', nextProps, prevState)

        return prevState
    }

    componentDidUpdate(){
        console.log('didupdate')
    }

    getSnapshotBeforeUpdate()
    {
        console.log('beforeUpdate')
    }

    componentWillUnmount()
    {
        console.log('Unmounted')
    }
    

    render(){
        console.log('render')

        // if (Math.random() > 0.7){
        //     throw new Error('Car random failed')
        // }
        const inputClasses = ['input']

        if (this.props.name !== ''){
            inputClasses.push('green')
        }else{
            inputClasses.push('red')
        }
    
        if (this.props.name.length > 4)
        {
            inputClasses.push('bold')
        }
    
        const style = {
            border: '1px solid #ccc',
            boxShadow: '0 4px 5px 3px rgba(0,0,0, .14)',
            ':hover': {
                border: '1px solid #aaa',
                boxShadow: '0 4px 15px 0 rgba(0,0,0, .25)',
                cursor: 'pointer'
            }
        }
        
        return (
            <div className="Car" style={style}>
                <h2>Car name: {this.props.name}</h2>
                <strong>Year: {this.props.year}</strong>
                <br />
                <input 
                    type="text" 
                    value={this.props.name}
                    placeholder="Without spaces" 
                    onChange={this.props.onChangeName}
                    className={inputClasses.join(' ')} 
                />
                <br />
                <button onClick={this.props.onDelete}>Delete</button>
            </div>
        )
    }
}

// const Car = props => {
  
// }



export default Radium(Car)